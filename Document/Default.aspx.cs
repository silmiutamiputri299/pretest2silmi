﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Document_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                LoadData();
            }
        }
    }

    private void LoadData()
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            repeaterDocument.DataSource = db.TBDocuments.Select(x => new
            {
                x.ID,
                x.UID,
                x.IDCompany,
                NameCompany = x.TBCompany.Name,
                x.IDCategory,
                NameCategory = x.TBDocumentCategory.Name,
                x.Name,
                x.Description,
                x.Flag,
                x.CreatedBy
            }).ToArray();
            repeaterDocument.DataBind();
        }
    }

    protected void repeaterDocument_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Document/Form.aspx?id=" + e.CommandArgument);
            }
            else if (e.CommandName == "Delete")
            {
                var document = db.TBDocuments.FirstOrDefault(x => x.ID == e.CommandArgument.ToInt());

                db.TBDocuments.DeleteOnSubmit(document);
                db.SubmitChanges();

                LoadData();
            }
        }
    }
}