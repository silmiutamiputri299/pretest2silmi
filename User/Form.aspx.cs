﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);
                listCompany.Items.AddRange(controllerCompany.DropDownList());

                ControllerPosition controllerPosition = new ControllerPosition(db);
                listPosition.Items.AddRange(controllerPosition.DropDownList());

                ControllerUser controllerUser = new ControllerUser(db);
                var user = controllerUser.Cari(Request.QueryString["id"].ToInt());

                if (user != null)
                {
                    listCompany.SelectedValue = user.IDCompany.ToString();
                    listPosition.SelectedValue = user.IDPosition.ToString();
                    InputName.Text = user.Name;
                    InputAddress.Text = user.Address;
                    InputEmail.Text = user.Email;
                    InputTelephone.Text = user.Telephone;
                    InputUsername.Text = user.Username;
                    InputPassword.Text = user.Password;
                    InputRole.Text = user.Role;


                    btnOk.Text = "Update";
                    LabelTitle.Text = "Update User";
                }
                else
                {
                    btnOk.Text = "Add New";
                    LabelTitle.Text = "Add New User";
                }
            }
        }
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerUser controllerUser = new ControllerUser(db);

                if (btnOk.Text == "Add New")
                {
                    controllerUser.Create(int.Parse(listCompany.SelectedValue), int.Parse(listPosition.SelectedValue), InputName.Text,
                        InputAddress.Text, InputEmail.Text, InputTelephone.Text, InputUsername.Text, InputPassword.Text, InputRole.Text);
                }
                else if (btnOk.Text == "Update")
                    controllerUser.Update(Request.QueryString["id"].ToInt(), int.Parse(listCompany.SelectedValue), int.Parse(listPosition.SelectedValue),
                        InputName.Text,
                        InputAddress.Text, InputEmail.Text, InputTelephone.Text, InputUsername.Text, InputPassword.Text, InputRole.Text);



                db.SubmitChanges();

                Response.Redirect("/User/Default.aspx");
            }
        }
    }
}